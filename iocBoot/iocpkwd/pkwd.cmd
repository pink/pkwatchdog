#!../../bin/linux-x86_64/pkwd

## You may have to change pkwd to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/pkwd.dbd"
pkwd_registerRecordDeviceDriver pdbbase

#epicsEnvSet ("IOCBL", "PINK")
#epicsEnvSet ("IOCDEV", "PKWD")

## Load record instances
dbLoadRecords("$(EXSUB)/db/exsub.db","P=$(IOCBL):$(IOCDEV):,R=exsub")

cd "${TOP}/iocBoot/${IOC}"

dbLoadTemplate("pkwd.substitutions", "BL=$(IOCBL),DEV=$(IOCDEV)")

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"

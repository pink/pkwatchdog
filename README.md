# pkwatchdog

EPICS IOC with basic watchdog functionality

## docker-composer.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink/pkwatchdog/pkwd:v1.0
    container_name: pkwd
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/pkwatchdog/iocBoot/iocpkwd"
    command: "./pkwd.cmd"
    volumes:
      - type: bind
        source: "/EPICS/autosave/pkwd"
        target: "/EPICS/autosave"
    environment:
      - IOCBL=PINK
      - IOCDEV=PKWD
```
